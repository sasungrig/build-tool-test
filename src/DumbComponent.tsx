import React, { Component } from 'react'
import { findIndex } from 'lodash-es'

interface IDumbComponentProps {} 

class DumbComponent extends Component<IDumbComponentProps> {

    welcomeMessage: string

    constructor(props) {
        super(props)

        this.welcomeMessage = 'Hello world'
    }

    render () {
        const arr = []
        const index = findIndex(arr, () => true)
        const theWelcomeMessage = `The welcome message is ${this.welcomeMessage}`
        return (
            <div>{theWelcomeMessage}{index}</div>
        )
    }
} 

export { DumbComponent }