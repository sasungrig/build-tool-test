import babel from 'rollup-plugin-babel'
import commonjs from 'rollup-plugin-commonjs'
import resolve from 'rollup-plugin-node-resolve'
import peerDepsExternal from 'rollup-plugin-peer-deps-external'
import { terser } from "rollup-plugin-terser"

const dist = './dist/';

export default {
    input: './src/index.ts',
    output: [
        {
            file: `${dist}output.cjs.js`,
            format: 'cjs'
        },
        {
            file: `${dist}output.esm.js`,
            format: 'esm'
        },
    ],
    plugins: [
        // NOTE: the order of the plugins MATTERS. See https://stackoverflow.com/questions/52884278/rollup-react-not-compiling-jsx
        peerDepsExternal({
            includeDependencies: true
        }),
        resolve({
            extensions: [ '.js', '.jsx', '.ts', '.tsx' ],
        }),
        babel({
            // only transpile our source code,
            exclude: 'node_modules/**',
            extensions: [ '.js', '.jsx', '.ts', '.tsx' ]
        }),
        commonjs(
            {
                sourceMap: false
            }
        ),
        terser()
    ]
};
